# Artillery #

An IDE to test Barrage script

To build Artillery, you'll need some others furusystems lib


### Barrage ###

Available at https://github.com/furusystems/Barrage


### flywheel ###

Available at https://github.com/furusystems/flywheel


### FSignal ###

Available at https://github.com/furusystems/FSignal

### HXComp ###

Available at https://github.com/Sunjammer/HXComp

##
After cloning/downloading, install these libs using 

```
#!batch

haxelib dev <libname> <libpath>
```     

##    
If you need FlashPlayer for desktop, download "Flash Player projector" at 
https://www.adobe.com/support/flashplayer/downloads.html  

Open the .hxproject with FlashDevelop and run Release