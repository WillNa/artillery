package com.furusystems.artillery;
import haxe.Resource;


class Macros
{
	public static function getFileContent( fileName : String )
	{
		return ( Resource.getString(fileName) );
	}
	
	/*
	macro public static function getFileContent( fileName : Expr ) {
		Context.registerModuleDependency("com.furusystems.artillery.Macros", "D:/github/Barrage/examples/dev.brg");
        var fileStr = null;
        switch( fileName.expr ) {
        case EConst(c):
            switch( c ) {
            case CString(s): fileStr = s;
            default:
            }
        default:
        };
        if( fileStr == null )
            Context.error("Constant string expected",fileName.pos);
        return Context.makeExpr(sys.io.File.getContent(fileStr),fileName.pos);
    }
	*/
}